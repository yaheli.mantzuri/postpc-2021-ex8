package com.example.ex8;


import org.junit.runner.RunWith;

import android.app.Application;
import android.content.Context;
//import android.content.res.Configuration;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;
import androidx.test.InstrumentationRegistry;
import androidx.test.core.app.ApplicationProvider;
import androidx.work.Configuration;
import androidx.work.WorkManager;
import androidx.work.impl.utils.SynchronousExecutor;
import androidx.work.testing.WorkManagerTestInitHelper;


import com.example.ex8.workers.MyWorker;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import junit.framework.TestCase;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.android.controller.ActivityController;
import org.robolectric.annotation.Config;
import org.robolectric.annotation.LooperMode;

import java.util.ArrayList;

import static junit.framework.TestCase.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.robolectric.Shadows.shadowOf;


@RunWith(RobolectricTestRunner.class)
@Config(sdk = 28, application = Application.class)

@LooperMode(LooperMode.Mode.PAUSED)
public class CalcTest extends TestCase {
    private ActivityController<MainActivity> activityController;
    private CalcHolder mockHolder;

    @Before
    public void setup() {
        Context context = ApplicationProvider.getApplicationContext();
        Configuration config = new Configuration.Builder()
                .setMinimumLoggingLevel(Log.DEBUG)
                .setExecutor(new SynchronousExecutor())
                .build();

        // Initialize WorkManager for instrumentation tests.
        WorkManagerTestInitHelper.initializeTestWorkManager(
                context, config);


        mockHolder = Mockito.mock(CalcHolder.class);
        activityController = Robolectric.buildActivity(MainActivity.class);


        MainActivity activityUnderTest = activityController.get();
        activityUnderTest.holder = mockHolder;


    }

    @Test
    public void test_1() {
        activityController.create().visible();
        MainActivity activityNewOrderTest = activityController.get();
        EditText description = activityNewOrderTest.findViewById(R.id.editTextInsertTask);
        String userInput = description.getText().toString();

        assertTrue(userInput.isEmpty());
    }

    @Test
    public void test_2() {
        activityController.create().visible();
        MainActivity activityNewOrderTest = activityController.get();
        EditText num = activityNewOrderTest.findViewById(R.id.editTextInsertTask);
        com.google.android.material.floatingactionbutton.FloatingActionButton b =
                activityNewOrderTest.findViewById(R.id.buttonCalc);
        num.setText("asds");
        assertFalse(b.isEnabled());
    }


    @Test
    public void test_3() {
        activityController.create().visible();
        MainActivity activityNewOrderTest = activityController.get();
        EditText num = activityNewOrderTest.findViewById(R.id.editTextInsertTask);
        com.google.android.material.floatingactionbutton.FloatingActionButton b =
                activityNewOrderTest.findViewById(R.id.buttonCalc);
        num.setText("5");
        assertTrue(b.isEnabled());
    }


}
