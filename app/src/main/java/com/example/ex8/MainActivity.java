package com.example.ex8;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.Data;
import androidx.work.WorkInfo;
import androidx.work.WorkManager;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    public CalcHolder holder = null;
    public CalcAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (holder == null) {
            holder =  MyWorkManagerApp.getInstance().getHolder();
        }

        WorkManager workManager = WorkManager.getInstance(this);


        LiveData<List<WorkInfo>> allCalc = workManager.getWorkInfosByTagLiveData("calc");
        allCalc.observe(this, new Observer<List<WorkInfo>>() {
            @Override
            public void onChanged(List<WorkInfo> workInfos) {
                for (WorkInfo workInfo: workInfos){
                    if (workInfo.getState() == WorkInfo.State.RUNNING){
                        Data progress = workInfo.getProgress();
                        int prog = progress.getInt("prog", 0);
                        double total = progress.getDouble("total", 0);
                        String id = progress.getString("id");
                        Calc calc = holder.getById(id);
                        if (calc != null) {
                            calc.total = total;
                            if (prog == 0) {
                                calc.progress = 1;
                            } else {
                                calc.progress = prog;
                            }
                            holder.sort_list();
                            holder.updateSp(id);
                            adapter.setItemToDo(holder.listCalcItems);
                        }
                    } else if (workInfo.getState() == WorkInfo.State.SUCCEEDED){
                        Data p = workInfo.getOutputData();
                        String id  = p.getString("id");
                        long root1  = p.getLong("root1", 0);
                        long root2  = p.getLong("root2", 0);
                        Calc calc = holder.getById(id);
                        if (calc != null){
                            calc.first = root1;
                            calc.sec = root2;
                            calc.status = "done";
                            holder.sort_list();
                            holder.updateSp(id);
                            adapter.setItemToDo(holder.listCalcItems);
                        }
                    }
                }
            }
        });



        adapter = new CalcAdapter(holder);
        adapter.setItemToDo(holder.getCurrentItems());
        RecyclerView recyclerView = findViewById(R.id.recyclerCalc);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        EditText editTextUserInput = findViewById(R.id.editTextInsertTask);
        FloatingActionButton buttonCalc = findViewById(R.id.buttonCalc);

        editTextUserInput.setText("");
        editTextUserInput.setEnabled(true);
        buttonCalc.setEnabled(false);

        editTextUserInput.addTextChangedListener(new TextWatcher() {
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
            public void afterTextChanged(Editable s) {
                String userInputString = editTextUserInput.getText().toString();
                buttonCalc.setEnabled(!userInputString.equals("") && isNumeric(userInputString));
            }
        });

        buttonCalc.setOnClickListener(v -> {
            String userInputString = editTextUserInput.getText().toString();
            if (!userInputString.equals("")){
                String id = holder.addNewInProgressItem(userInputString);
                Calc calc = holder.getById(id);
                editTextUserInput.setText("");
                adapter.setItemToDo(holder.getCurrentItems());
                recyclerView.setAdapter(adapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));


                holder.makeReq(workManager, userInputString, id, calc.progress);
            }
        });
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}