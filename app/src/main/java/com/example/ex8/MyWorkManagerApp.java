package com.example.ex8;

import android.app.Application;

import androidx.work.Data;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.example.ex8.workers.MyWorker;

import java.util.Date;

public class MyWorkManagerApp extends Application{


    private CalcHolder calcHolder;

    public CalcHolder getHolder() {
        return calcHolder;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        calcHolder = new CalcHolder(this);
    }


    private  static MyWorkManagerApp instance = null;

    public static MyWorkManagerApp getInstance() {
        return instance;
    }
}
