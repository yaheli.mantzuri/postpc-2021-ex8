package com.example.ex8.workers;


import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.work.Data;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.ex8.Calc;
import com.example.ex8.CalcHolder;
import com.example.ex8.MyWorkManagerApp;

public class MyWorker extends Worker {
    public CalcHolder holder = null;

    public MyWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);


        if (holder == null) {
            holder =  MyWorkManagerApp.getInstance().getHolder();
        }
    }



    @NonNull
    @Override
    public Result doWork() {
        long timeStartMs = System.currentTimeMillis();
        long numberToCalculateRootsFor = getInputData().getLong("number", 0);
        if (numberToCalculateRootsFor <= 0) {
            Log.e("CalculateRootsService", "can't calculate roots for non-positive input" + numberToCalculateRootsFor);
        }

        long root1 = 1;
        long root2 = numberToCalculateRootsFor;
        boolean flag = false;
        double time = 0;
        String id = getInputData().getString("id");
        int p = getInputData().getInt("progress", 2);





        Calc calc = holder.getById(id);
        int i;

        if (calc.getProgress()>2){
            i = calc.getProgress();
        } else {
            i = 2;
        }



        for (; i <= Math.sqrt(numberToCalculateRootsFor); i++) {
            if (this.isStopped()){
                break;
            }
            long end = System.currentTimeMillis();
            time = (((double) (end - timeStartMs)) / 1000)/60;
            if (time <= 15) {
                if (numberToCalculateRootsFor % i == 0) {
                    if (numberToCalculateRootsFor / i == i) {
                        root1 = i;
                        root2 = i;
                    } else {
                        root1 = i;
                        root2 = numberToCalculateRootsFor / i;
                    }
                    break;
                }
            } else {
                break;
            }

            if((i % 1000) == 0){
                setProgressAsync(
                        new Data.Builder()
                                .putString("id", id)
                                .putInt("prog", i)
                                .putDouble("total", Math.sqrt(numberToCalculateRootsFor))
                                .build()
                );
            }

        }


        return Result.success(

                new Data.Builder()

                        .putString("id", id)
                        .putLong("root1", root1)
                        .putLong("root2", root2)
                        .build()
        );
    }

}