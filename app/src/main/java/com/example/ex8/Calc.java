package com.example.ex8;

import java.util.Comparator;
import java.util.UUID;

public class Calc {
    String number;
    long first;
    long sec;
    String status;
    String id;
    int progress = 0;
    double total;


    public int getProgress() {
        return progress;
    }
}


class sortItems implements Comparator<Calc> {

    @Override
    public int compare(Calc o1, Calc o2) {
        if (o1.status.equals("done") && o2.status.equals("done")){
            return 0;
        }
        if (o1.status.equals("done")){
            return 1;
        }
        if (o2.status.equals("done")){
            return -1;
        }
        if (!o1.status.equals("done") && !o2.status.equals("done")) {
            long first = Long.parseLong(o2.number);
            long sec = Long.parseLong(o1.number);
            if (first > sec){
                return -1;
            } else if(first < sec){
                return 1;
            } else {
                return 0;
            }
        }
        return 0;
    }
}

