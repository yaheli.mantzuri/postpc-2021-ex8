package com.example.ex8;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.work.Data;
import androidx.work.ExistingWorkPolicy;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;

import com.example.ex8.workers.MyWorker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.UUID;

public class CalcHolder {
    public List<Calc> listCalcItems = new ArrayList<>();
    private final Context context;
    private final SharedPreferences sp;
    WorkManager workManager;

    public CalcHolder(Context context){
        this.context = context;
        this.sp = context.getSharedPreferences("local_db", Context.MODE_PRIVATE);
        initializeFromSp();
        Collections.sort(listCalcItems, new sortItems());
        workManager = WorkManager.getInstance(context);
    }

    private void initializeFromSp(){
        Set<String> keys = sp.getAll().keySet();

        for (String key :keys){
            String calcSave = sp.getString(key, null);

            Calc calc = getCalcFromString(calcSave);
            if (calc != null){
                listCalcItems.add(calc);
            }
        }
    }


    public String getStringfromCalc(Calc calc){
        return calc.number + "#" + calc.first + "#" + calc.sec + "#" + calc.status + "#" + calc.id + "#" + calc.progress + "#" + calc.total;
    }

    public Calc getCalcFromString(String str){
        if (str == null){
            return null;
        }

        String[] arrOfStr1 = str.split("#", 7);
        Calc calc = new Calc();
        calc.number = arrOfStr1[0];
        calc.first = Long.parseLong(arrOfStr1[1]);
        calc.sec = Long.parseLong(arrOfStr1[2]);
        calc.status = arrOfStr1[3];
        calc.id = arrOfStr1[4];
        calc.progress = Integer.parseInt(arrOfStr1[5]);
        calc.total = Double.parseDouble(arrOfStr1[6]);

        return calc;
    }


    public ArrayList<Calc> getCurrentItems() {
        ArrayList<Calc> copy = new ArrayList<>();
        copy.addAll(listCalcItems);
        return copy;
    }


    public String addNewInProgressItem(String num) {
        Calc calc = new Calc();
        calc.id =  UUID.randomUUID().toString();
        calc.number = num;
        calc.status = "IN-PROGRESS";
        listCalcItems.add(0,calc);
        Collections.sort(listCalcItems, new sortItems());
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(calc.id, getStringfromCalc(calc));
        editor.apply();

        return calc.id;
    }

    public void deleteItem(Calc item) {
        listCalcItems.remove(item);
        Collections.sort(listCalcItems, new sortItems());

        SharedPreferences.Editor editor = sp.edit();
        editor.remove(item.id);
        editor.apply();
    }

    public Calc getById(String id){
        for (Calc calc : listCalcItems ){
            if (calc.id.equals(id)){
                return calc;
            }
        }
        return null;
    }


    public void updateSp(String id){
        Calc calc = getById(id);

        SharedPreferences.Editor editor = sp.edit();
        editor.putString(calc.id, getStringfromCalc(calc));
        editor.apply();
    }

    public void sort_list(){
        Collections.sort(listCalcItems, new sortItems());
    }

    public void makeReq(WorkManager workManager, String input, String id, int progress){
//        this.workManager = workManager;
        Calc calc = getById(id);

        OneTimeWorkRequest req = new OneTimeWorkRequest.Builder(MyWorker.class)
                .addTag(calc.number)
                .addTag("calc")
                .setInputData(
                        new Data.Builder()
                                .putLong("number", Long.parseLong(input))
                                .putString("id", id)
                                .putInt("progress", progress)
                                .build()
                )
                .build();


        workManager.enqueue(req);

    }

    public void stopWorker(String num){

        workManager.cancelAllWorkByTag(num);
    }

}


