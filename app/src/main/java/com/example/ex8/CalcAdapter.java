package com.example.ex8;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.work.WorkManager;

import java.util.ArrayList;
import java.util.List;

public class CalcAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Calc> _listCalc= new ArrayList<>();
    CalcHolder _calcHolder;
    private static final int LAYOUT_ONE= 0;
    private static final int LAYOUT_TWO= 1;



    public CalcAdapter(CalcHolder calcHolder){
        this._calcHolder = calcHolder;
    }


    @Override
    public int getItemViewType(int position)
    {
        Calc calc = _listCalc.get(position);
        if(calc.status.equals("IN-PROGRESS"))
            return LAYOUT_ONE;
        else
            return LAYOUT_TWO;
    }


    public void setItemToDo(List<Calc> list_items){
        _listCalc.clear();
        _listCalc.addAll(list_items);
        notifyDataSetChanged();
    }




    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view =null;
        RecyclerView.ViewHolder viewHolder = null;

        if(viewType==LAYOUT_ONE)
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_calc,parent,false);
            viewHolder = new holderItemsCalc(view);
        }
        else
        {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_after_calc,parent,false);
            viewHolder= new holderItemsAfterCalc(view);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        if(holder.getItemViewType()== LAYOUT_ONE)
        {
            Calc calc = _listCalc.get(position);
            holderItemsCalc holderItemsCalc = (holderItemsCalc) holder;

            holderItemsCalc.description.setText("calculating roots for "+ calc.number);
            holderItemsCalc.progress.setMax((int)calc.total);
            holderItemsCalc.progress.setProgress(calc.progress);

            holderItemsCalc.cancel.setOnClickListener(v -> {
                _calcHolder.deleteItem(calc);
                setItemToDo(_calcHolder.getCurrentItems());
                _calcHolder.stopWorker(calc.number);
            });

        }
        else {

            Calc calc = _listCalc.get(position);
            holderItemsAfterCalc holderItemsAfterCalc = (holderItemsAfterCalc) holder;

            holderItemsAfterCalc.description.setText("roots for "+ calc.number + ": "+calc.first + "*"+ calc.sec);

            holderItemsAfterCalc.delete.setOnClickListener(v -> {
                _calcHolder.deleteItem(calc);
                setItemToDo(_calcHolder.getCurrentItems());
            });
        }

    }

    @Override
    public int getItemCount() {
        return _listCalc.size();
    }
}




class holderItemsCalc extends RecyclerView.ViewHolder{
    TextView description;
    ImageButton cancel;
    ProgressBar progress;

    public holderItemsCalc(View view) {
        super(view);
        description = view.findViewById(R.id.description);
        progress = view.findViewById(R.id.progressBar);
        cancel = view.findViewById(R.id.imageButton);;
    }
}


class holderItemsAfterCalc extends RecyclerView.ViewHolder{
    TextView description;
    ImageButton delete;

    public holderItemsAfterCalc(View view) {
        super(view);
        description = view.findViewById(R.id.description);
        delete = view.findViewById(R.id.imageButton);

    }
}